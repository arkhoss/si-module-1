# Demo for Protocols DNS, SMTP and POP/IMAP Roundcube
# Demo for Protocols NMS, SNMP and Zabbix


## Objective
---
This repository describes the steps required to setup 3 servers, 1 master for DNS manager, 1 application server for mail services SMTP, Dovecot and 1 application server for roundcube, all for development purposes. This setup provides a production-like setup in your local machine.

<br>

## Why use Vagrant and Ansible?
---

Vagrant is a tool that will allow us to create a virtual environment easily and it eliminates pitfalls that cause the works-on-my-machine phenomenon. It can be used with multiple providers such as Oracle VirtualBox, VMware, Docker, and so on. It allows us to create a disposable environment by making use of configuration files.

Ansible is an infrastructure automation engine that automates software configuration management. It is agentless and allows us to use SSH keys for connecting to remote machines. Ansible playbooks are written in yaml and offer inventory management in simple text files.

## Prerequisites

- Vagrant should be installed on your machine. Installation binaries can be found [Vagrant Download](https://www.vagrantup.com/downloads.html).
Oracle VirtualBox can be used as a Vagrant provider or make use of similar providers as described in Vagrant’s official documentation.
- Ansible should be installed in your machine. Refer to the Ansible installation guide for platform specific installation.

<br>

## Bring everything up
```
make install
```

## Bring VMs up
```
make infra-vagrant
```

## Configure the VMs
```
make platform
```

## Access to webmail

### outside VMs network
[webmail](http://10.11.0.252)

### inside VMs network
[webmail](http://webmail.acme.local)


## Access to Monitor / Zabbix

### outside VMs network
[monitor](http://10.11.0.250)

### inside VMs network
[monitor](http://monitor.acme.local)

### Access for webmail

```
User: centos

Pass: 1234Cent.
```

### Ansible .vault_pass

```
dK$V5n3c68j3#7$!%YJ%
```

## In case the automated make install fails

This instructions is just in case the automated make install command fails. So, to run it manually you can go like this:

### Ansible - Installing Platform for masters

```
ansible-playbook ../ansible/si-master.yml -i inventories/si-env --limit='master' --private-key="../mykey" --vault-password-file=.vault_pass -e "zabbix_first_run=true" -u centos
```

### Ansible - Installing Platform for apps

```
ansible-playbook ../ansible/si-apps.yml -i inventories/si-env --limit='apps' --private-key="../mykey" --vault-password-file=.vault_pass -u centos
```


## For Zabbix stuff

This instructions is just in case the automated make install command fails for zabbix only. So, to run it manually you can go like this:

### First Run

```
ansible-playbook  ../ansible/site.yml -i inventories/si-env --limit="master" --private-key="../mykey"  --vault-password-file=.vault_pass -e "zabbix_first_run=true"  -u centos   -t zabbix
```

### After first Run

```
ansible-playbook  ../ansible/site.yml -i inventories/si-env --limit="master" --private-key="../mykey"  --vault-password-file=.vault_pass -u centos   -t zabbix
```

### Access for Monitor / Zabbix

```
User: Admin

Pass: zabbix
```

