#!/bin/bash
# Usage: vagrant-deploy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

# generate key for vagrant user
if [ ! -f "mykey" ]; then
    ssh-keygen -t rsa -N "" -f mykey
fi

cd vagrant/

pwd

mkdir -p src
cp -r ../mykey.pub src/

vagrant status

vagrant up

vagrant status

cd ..

echo "Vagrant Completed"

