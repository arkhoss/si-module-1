#!/bin/bash
# Usage: vagrant-destroy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

cd vagrant/

pwd

vagrant status

vagrant destroy -f

sleep 5s

vagrant status

cd ..

echo "Vagrant Destroyed"

