#!/bin/bash
# Usage: terraform-deploy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

echo "Ansible Inventory"
cat ansible/inventories/si-env

# ONLY WSL USERS
# if this fails, is because permissions are bad in the Filesystem,
# run commands below to make it work, you need to use a different drive like.. D:
# sudo umount /mnt/d
# sudo mount -t drvfs D: /mnt/d -o metadata
echo "Key permissions"
chmod 600 mykey

cd ansible

#echo "Sleeping 1min"
#sleep 1m

echo "Ansible - Installing Platform for masters"
ansible-playbook ../ansible/si-master.yml -i inventories/si-env --limit='master' --private-key="../mykey" --vault-password-file=.vault_pass -e "zabbix_first_run=true" -u centos

echo "Ansible - Installing Platform for apps"
ansible-playbook ../ansible/si-apps.yml -i inventories/si-env --limit='apps' --private-key="../mykey" --vault-password-file=.vault_pass -u centos

cd ..

echo "Platform Installed"
