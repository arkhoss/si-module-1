#!/bin/bash
# Usage: clean up repo
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

rm -rf mykey*

rm -rf vagrant/.vagrant

rm -rf vagrant/src


