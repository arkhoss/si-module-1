#!/bin/bash
# Usage: bootstrap.sh
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0
# Description: setup initial settings for si env

# safe pipefail
set -euo pipefail

# generate key for vagrant user
if [ ! -f "/home/vagrant/.ssh/id_rsa" ]; then
  ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
fi
cp /home/vagrant/.ssh/id_rsa.pub /vagrant/vagrant.pub

chown -R vagrant:vagrant /home/vagrant/.ssh/

id -u centos &>/dev/null || useradd -s /bin/bash -m -k /etc/skel -c "centos admin" -p "5uYn6q3B6yKlw" centos

usermod -aG wheel centos
mkdir -p /home/centos/.ssh/
chown -R centos:centos /home/centos/.ssh/
cat /vagrant/src/mykey.pub >> /home/centos/.ssh/authorized_keys


cat << 'SSHEOF' > /etc/sudoers
root  ALL=(ALL) ALL
vagrant  ALL=(ALL) NOPASSWD:ALL
centos  ALL=(ALL) NOPASSWD:ALL
SSHEOF

cat << 'SSHEOF' > /etc/selinux/config
SELINUX=disabled
SELINUXTYPE=targeted
SSHEOF

echo "AllowUsers centos vagrant" >> /etc/ssh/sshd_config
systemctl restart sshd

