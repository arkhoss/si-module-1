# =========================================== SI modules ============================================

# ----------------------- SETUP -------------------------------------------------------------------
# Will deploy a given instance/server
platform:
	@echo "== Installing Platform =="
	@./scripts/ansible-playbooks.sh

# Will install a server using vagrant in local VirtualBox
infra-vagrant:
	@echo "== Installing Infrastructure with Vagrant =="
	@./scripts/vagrant-deploy.sh

destroy-vagrant:
	@echo "== Destroy All Deployed Resources in Vagrant =="
	@./scripts/vagrant-destroy.sh

clean-up:
	@echo "cleaning previous run if any"
	@./scripts/clean-up.sh

install: clean-up infra-vagrant platform

